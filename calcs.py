import math

def win_expectancy(rate_diff):
    '''
    Calculates the win expectancy for a team.

    :param rate_diff: Difference in rating between the teams
    '''
    return 1 / (pow(10, (-rate_diff / 400)) + 1)


def margin_of_victory_modifier(ELOW, ELOL, PD):
    '''
    Determines the modifer for the margin of victory.
    
    :param ELOW: Winning team ELO before the game
    :param ELOL: Losing team ELO before the game
    :param PD: Point difference in the game
    '''
    return (math.log((abs(PD) + 1)) * (2.2/((ELOW - ELOL) * 0.001 + 2.2))) 


def elo(movm, outcome, we, r_old=1300, k=20 ):
    '''
    Determines the new ELO rating for a team.

    :param movm: Margin of Victory modifier
    :param outcome: Outcome of game: 1 for win, 0.5 for tie, 0 for loss
    :param we: Win expectancy
    :param r_old: ELO rating before the game (defaults to 1300 if team didn't have ELO)
    :param k: K Value (defaults to 20)
    '''
    return (r_old + ((k * movm) * (outcome - we)))
    

# Test of determining Team A's new ELO after beating Team B
team_a_elo = 1500
team_b_elo = 1425
rate_diff = team_a_elo - team_b_elo
we = win_expectancy(rate_diff)
movm = margin_of_victory_modifier(team_a_elo, team_b_elo, 10)
new_elo = elo(movm, 1, we, team_a_elo)
print(new_elo)
