import http.client
import json
import psycopg2 as pg
import dotenv


def main():
    dotenv.load()
    key = dotenv.get('SPORTRADAR_KEY')
    # conn = create_connection()
    data = get_api_data(key)

    # division = get_division(data)
    # conferences = get_conferences(data)
    teams = get_teams(data)
    venues = get_venues(teams)

    # insert_conferences(conn, data)
    # insert_divisions(conn, data)

    # conn.close()
    print(venues)


def create_connection():
    '''
    Opens a psycopg2 connection to be used.

    :returns: psycopg2 connection object
    '''
    try:
        dbname = dotenv.get('PG_DATABASE')
        host = dotenv.get('PG_HOST')
        user = dotenv.get('PG_USERNAME')
        password = dotenv.get('PG_PASSWORD')

        conn = pg.connect("dbname=" + dbname + " user=" +
                          user + " password=" + password + " host=" + host)

        return conn

    except pg.DatabaseError as e:
        print(f"Could not connect to the DB! Error {e}")


def insert_conferences(conn, data):
    '''
    Inserts conferences retrieved from the Sportradar API.

    :param conn: A psycopg2 database connection
    :param data: A dictionary of the complete data from the API 
    '''
    if conn is not None:
        try:
            conf = get_conferences(data)

            cur = conn.cursor()
            cur.executemany(
                """INSERT INTO conferences (name) VALUES (%(name)s) ON CONFLICT (name) DO NOTHING""", conf)
            conn.commit()
            print("Transaction committed!")
        except pg.DatabaseError as e:
            conn.rollback()
            print(f"Insert failed with error {e}. Transaction rolled back!")

        cur.close()
    else:
        print("Connection was empty!")


def insert_divisions(conn, data):
    '''
    Inserts divisions retrieved from the Sportradar API.

    :param conn: A psycopg2 database connection
    :param data: A dictionary of the complete data from the API 
    '''
    if conn is not None:
        try:
            division = get_division(data)

            cur = conn.cursor()
            cur.execute(
                """INSERT INTO divisions (name, alt_name) VALUES (%(name)s, %(alt_name)s) ON CONFLICT (name) DO NOTHING""", division)
            conn.commit()
            print("Transaction committed!")
        except pg.DatabaseError as e:
            conn.rollback()
            print(f"Insert failed with error {e}. Transaction rolled back!")

        cur.close()
    else:
        print("Connection was empty!")


def get_api_data(api_key):
    '''
    Connects to the Sportradar API.

    :param api_key: Key to connect to Sportradar API
    :returns: A dictionary of the complete Team Hierarchy endpoint response
    '''
    conn = http.client.HTTPSConnection("api.sportradar.us")

    conn.request(
        "GET", "/ncaafb-t1/teams/FBS/hierarchy.json?api_key=" + api_key)

    res = conn.getresponse()
    data = res.read()

    fbs_team_data = data.decode("utf-8")
    parsed_fbs_data = json.loads(fbs_team_data)

    return parsed_fbs_data


def get_division(data):
    '''
    Gets the division information from the Team Hierarchy endpoint dictionary.

    :param data: Dictionary of the Team Hierarchy endpoint
    :returns: A dictionary of the division information
    '''

    return { 'name': data['id'], 'alt_name': data['name'] }


def get_conferences(data):
    '''
    Gets the conferences information from the Team Hierarchy endpoint dictionary.

    :param data: Dictionary of the Team Hierarchy endpoint
    :returns: A tuple of the conferences information
    '''

    return tuple(c['name'] for c in data['conferences'])


def get_teams(data):
    '''
    Gets the teams information from the Team Hierarchy endpoint dictionary.

    :param data: Dictionary of the Team Hierarchy endpoint
    :returns: A list of the teams information
    '''

    conferences = data['conferences']
    teams = []

    for c in conferences:
        if 'subdivisions' in c:
            for s in c['subdivisions']:
                teams.append(s['teams'])
        else:
            teams.append(c['teams'])


    # teams = [c if 'subdivisions' not in c 
    #        else [s['teams'] for s in c['subdivisions']] 
    #        for c in data['conferences']] 

    return teams


def get_venues(teams):
    '''
    WIP
    TODO: Determine how to pull the venue information from the teams list
    '''
    venues = [t['venue'] for c in teams for t in c]
    # for c in teams:
    #    for t in c:
    #        venues.append(t['venue'])

    return venues


if __name__ == "__main__":
    main()
