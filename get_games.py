import http.client
import json
import dotenv


def get_api_data():
    '''
    WIP
    TODO: Finish inserting team data into database with get_teams.py before
    continuing with this file.
    POC for connecting to Sportradar Schedule endpoint
    '''
    key = dotenv.get('SPORTRADAR_KEY')
    conn = http.client.HTTPSConnection("api.sportradar.us")

    conn.request("GET", "/ncaafb-t1/2018/REG/schedule.json?api_key=" + key)

    res = conn.getresponse()
    data = res.read()

    game_data = data.decode("utf-8")
    parsed_data = json.loads(game_data)

    with open("data/2018_Games.json", "w") as outfile:
        json.dump(parsed_data, outfile)

    print(parsed_data)

get_api_data()